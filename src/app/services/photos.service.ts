import { Injectable } from '@angular/core';
import { Photo } from '../interfaces/photos.i'
@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  
  constructor() { }

  async loadPhotoList(): Promise<Photo[]> {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/photos/')
      return await response.json() ?? []
      

    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
