import { Component, OnInit } from '@angular/core';
import { PhotosService} from '../../services/photos.service'
import { Photo } from '../../interfaces/photos.i'

@Component({
  selector: 'app-list-photos',
  templateUrl: './list-photos.component.html',
  styleUrls: ['./list-photos.component.scss']
})
export class ListPhotosComponent implements OnInit {

  photosList: Photo[] = []
  public searchValue: string = ''
  public orderSelected: string = ''
  public loadingList: boolean  = false
  public itemsByPage: number = 10
  public selectedPage: number = 1

  constructor(private photosService: PhotosService) { }

  async ngOnInit(): Promise<void> {
    await this.loadPhotos()
  }

  async loadPhotos() {
    console.log('Loading Photos...')
    this.loadingList = true
    try {
      this.photosList = await this.photosService.loadPhotoList()
    
    } catch (error) {
      console.log(error)
    }
    this.loadingList = false
  }

  get photosListComputed(): Photo[] {
    let result = this.photosList
    if (this.searchValue.length > 0) {
      result = result.filter(item => JSON.stringify(item).toUpperCase().includes(this.searchValue.toUpperCase()))
    }
    return result
  }

  get currentPage(): Photo[] {
    return this.photosListComputed.slice((this.selectedPage - 1) * this.itemsByPage, this.selectedPage * this.itemsByPage)
  }

  get maxPages(): number {
    return Math.ceil(this.photosListComputed.length / this.itemsByPage)
  }

  changePage(i: number) : void {
    this.selectedPage  = this.selectedPage + i
  }

}
