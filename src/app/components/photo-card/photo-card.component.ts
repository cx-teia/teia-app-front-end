import { Component, OnInit, Input } from '@angular/core';
import { Photo } from '../../interfaces/photos.i'

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss']
})


export class PhotoCardComponent implements OnInit {
  @Input() photoData: Photo | undefined = undefined
  constructor() { }

  ngOnInit(): void {
    
  }

}
