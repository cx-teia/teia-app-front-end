import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.scss']
})
export class PageTitleComponent implements OnInit {
  @Input() title: string = 'Placeholder do Título'
  @Input() description: string | undefined = undefined

  constructor() { }

  ngOnInit(): void {
  }

}
